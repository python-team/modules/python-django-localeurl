python-django-localeurl (1.5-4) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  * Remove DM-Upload-Allowed; it's no longer used by the archive
    software.
  * Run tests only if DEB_BUILD_OPTIONS=nocheck is not set.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:04:24 -0500

python-django-localeurl (1.5-3) unstable; urgency=low

  * debian/source/options
    - Ignore django_localeurl.egg-info changes after build. (Closes: #671419)

 -- Janos Guljas <janos@resenje.org>  Tue, 12 Jun 2012 17:13:04 +0200

python-django-localeurl (1.5-2) unstable; urgency=low

  * Add patches/fix-tests-for-django-1.4.patch (Closes: #669484)
    - Fix tests for Django 1.4.
  * Add patches/fix-monkey-patching-urlresolvers.reverse.patch
    (Closes: #665908)
    - Workaround for monkey-patching urlresolvers.reverse Django function.
  * debian/copyright:
    - Fix dep5 syntax.
    - Update Format link.
  * Bump standards to 3.9.3.
  * Add DM-Upload-Allowed control field.

 -- Janos Guljas <janos@resenje.org>  Mon, 23 Apr 2012 18:33:37 +0200

python-django-localeurl (1.5-1) unstable; urgency=low

  * Initial release. (Closes: #572638)

 -- Janos Guljas <janos@resenje.org>  Thu, 25 Aug 2011 00:23:08 +0200
